<?php

/* 
 * @file
 * Theme functions for the uc_pic_cart_block module.
 */

/**
 * Theme the block title.
 *
 * Unlike standart block, we haven't collapsing.
 *
 * @param $variables
 *   An associative array containing:
 *   - title: A title text.
 *   - icon_class: 'empty' or 'full' will show appropriate icon, FALSE - won't.
 *   - path_module: Path to this module.
 * 
 * @return string
 *   The HTML output.
 */
function theme_uc_pic_cart_block_block_title($variables) {

  $title = $variables['title'];
  $icon_class = $variables['icon_class'];
  $path_module = $variables['path_module'];

  $output = '';

  if (empty($path_module)) {
    return $output;
  }

  if ($icon_class == 'empty') {
    $path = $path_module .'/img/cart_empty.gif';
  }
  elseif ($icon_class == 'full') {
    $path = $path_module .'/img/cart_full.gif';
  }

  if (!empty($path)) {
    $output = l(theme('image', array('path' => $path, 'alt' => '', 'title' =>  t('View your shopping cart'), 'attributes'  => array('class' => 'uc_pic_cart_block_btn'))), 'cart', array('html' => TRUE, 'attributes' => array('rel' => 'nofollow'))) .' ';
  }

  $output .= '<span class="uc_pic_cart_block_block_title">'. $title .'</span>';
  return $output;
}

/**
 * Theme the cachable block content - message to user.
 */
/*function theme_uc_pic_cart_block_content_cachable() {
  $output = t('We are sorry, but advanced cart block features are not available for guests with current site settings.');

  $attr = array('attributes' => array('rel' => 'nofollow'));

  $output .= ' '. t('Please') .' '. l(t('register'), 'user/register', $attr) .' '. t('or') .' ';
  $output .= l(t('login'), 'user/login', $attr) .'.';
  $output .= '<p>'. t('You can also') .' '. l(t('view your cart'), 'cart', $attr) .'.</p>';

  return $output;
}*/

/**
 * Theme the sort header - "Sort by: name | qty | sum"
 * 
 * @param $variables
 *   An associative array containing:
 *   - path_module: Path to this module.
 * 
 * @return string
 *   The HTML output.
 */
function theme_uc_pic_cart_block_sort_header($variables) {
  $path_module = $variables['path_module'];

  if (empty($path_module)) {
    return '';
  }

  $dest_url = drupal_get_destination();
  $attr = array('attributes' => array('rel' => 'nofollow' , 'class' => 'use-ajax'), 'query' => $dest_url);
  $sortorder = !empty($_SESSION['uc_pic_cart_block_sortorder']) ? $_SESSION['uc_pic_cart_block_sortorder'] : '';

  if (!is_numeric($sortorder)) {
    $sortorder = 0;
    $_SESSION['uc_pic_cart_block_sortorder'] = 0;
  }
  $output = '<p class="uc_pic_cart_block_sort_header">'. t('Sort by') .': ';

  if ($sortorder % 2) {
    $arrowup = ' '. theme('image', array('path' => $path_module .'/img/bullet-arrow-up.gif', 'alt' => t('d'), 'title' => t('Descending'), 'attributes' => array('class' => 'uc_pic_cart_block_btn')));
  }
  else {
    $arrowdown = ' '. theme('image', array('path' => $path_module .'/img/bullet-arrow-down.gif', 'alt' => t('a'), 'title' => t('Ascending'), 'attributes' => array('class' => 'uc_pic_cart_block_btn')));
  }

  $output .= l(t('name'), 'uccartpics/sort/nojs/'. (($sortorder==0) ? '1' : '0'), $attr);
  if ($sortorder == 0) {
    $output .= $arrowdown;
  }
  elseif ($sortorder == 1) {
    $output .= $arrowup;
  }

  $output .= ' | ';
  $output .= l(t('qty'), 'uccartpics/sort/nojs/'. (($sortorder==2) ? '3' : '2'), $attr);
  if ($sortorder == 2) {
    $output .= $arrowdown;
  }
  elseif ($sortorder == 3) {
    $output .= $arrowup;
  }

  $output .= ' | ';
  $output .= l(t('sum'), 'uccartpics/sort/nojs/'. (($sortorder==4) ? '5' : '4'), $attr);
  if ($sortorder == 4) {
    $output .= $arrowdown;
  }
  elseif ($sortorder == 5) {
    $output .= $arrowup;
  }

  $output .= "</p>\n";
  return $output;
}

/**
 * Theme the main block content (cart items).
 * 
 * @param $variables
 *   An associative array containing:
 *   - use_scroll: boolean
 *   - orientation: 0 is vertical, other is horisontal.
 *   - items: cart display items @see uc_pic_cart_block_content
 *   - path_module: Path to this module.
 * 
 * @return string
 *   The HTML output.
 */
function theme_uc_pic_cart_block_content($variables) {
  $use_scroll = $variables['use_scroll'];
  $orientation = $variables['orientation'];
  $items = $variables['items'];
  
  $output = '';
  
  if ($use_scroll) {
    $output .= theme('uc_pic_cart_block_scroll_btn', array(
      'last' => FALSE,
      'orientation' => $orientation,
    ));
  }

  if ($orientation == 0) { // vertical
    $output .= '<table id="uc_pic_cart_block_content"><tbody>';
  }
  else { // horisontal
    $output .= '<div id="uc_pic_cart_block_content">';
  }
  $output .= "\n";
  
  $rownum = 0;
  foreach ($items as $item) {
    $rownum += 1;

    $output .= theme('uc_pic_cart_block_item', array(
      'item' =>  $item,
      'rownum' => $rownum,
      'orientation' => $orientation,
    )) ."\n";
  }

  if ($orientation == 0) {
    $output .= "</tbody></table> <!-- cart content --> \n";
  }
  else {
    $output .= "</div> <!-- cart content --> \n";
  }

  if ($use_scroll) {
    $output .= theme('uc_pic_cart_block_scroll_btn', array(
      'last' => TRUE,
      'orientation' => $orientation,
    ));
  }
  
  return $output;
}

/**
 * Theme the scroll buttons.
 *
 * @param $variables
 *   An associative array containing:
 *   - last: Boolean. If false, we have the first part of scrolling (e.g. top),
 *           otherwise - last (e.g. bottom).
 *   - orientation: 0 is vertical, other is horisontal.
 *   - path_module: Path to this module.
 *
 * @return string
 *   The HTML output.
 */
function theme_uc_pic_cart_block_scroll_btn($variables) {

  $last = $variables['last'];
  $orientation = $variables['orientation'];
  $path_module = $variables['path_module'];

  $output = '';
  if (!is_bool($last) || !is_numeric($orientation) || empty($path_module)) {
    return $output;
  }

  if ($last) {
    $output .= "</div> <!-- scroll area -->\n";
  }

  if (!$last && ($orientation == 0)) {
    // scroll up
    $arrowup = theme('image', array('path' => $path_module . '/img/bullet-arrow-up.gif', 'alt' =>  '^', 'title' => '', 'attributes' => array('class' => 'uc_pic_cart_block_btn')));
    $output .= '<div id="uc_pic_cart_block_scroll_up" class="uc_pic_cart_block_scroll_up_def">'. $arrowup .'</div>';
  }
  elseif ($last && ($orientation == 0)) {
    // scroll down
    $arrowdown = theme('image', array('path' => $path_module . '/img/bullet-arrow-down.gif', 'alt' => 'v', 'title' => '', 'attributes' => array('class' => 'uc_pic_cart_block_btn')));
    $output .= '<div id="uc_pic_cart_block_scroll_down" class="uc_pic_cart_block_scroll_down_def">'. $arrowdown .'</div>';
  }
  elseif (!$last) {  
    // scrolling horisontal
    $arrowleft = theme('image', array('path' => $path_module . '/img/bullet-arrow-left.gif',  'alt' => '<', 'title' => '', 'attributes' => array('class' => 'uc_pic_cart_block_btn')));
    $arrowright = theme('image', array('path' => $path_module .'/img/bullet-arrow-right.gif', 'alt' => '>', 'title' => '', 'attributes' => array('class' => 'uc_pic_cart_block_btn')));
    $output .= '<div id="uc_pic_cart_block_scroll_left" class="uc_pic_cart_block_scroll_left_def">'. $arrowleft .'</div>';
    $output .= '<div id="uc_pic_cart_block_scroll_right" class="uc_pic_cart_block_scroll_right_def">'. $arrowright .'</div>';
  }

  if (!$last) {
    $output .= '<div id="uc_pic_cart_block_scroll_area">';
  }
  $output .= "\n";

  return $output;
}

/**
 * Theme the cart item in block.
 *
 * @param $variables
 *   An associative array containing:
 *   - item: Array which represents current cart item:
 *     - title: name of product (link to node in most cases),
 *     - description: description of product, e.g. attributes, parts of product kit, etc.
 *     - qty: quantity of product,
 *     - price: price of item,
 *     - nid: node ID of product,
 *     - ciid: cart item ID
 *     - data: serialized data of product,
 *     - img: full themed image of product,
 *     - module: module of product ('uc_product', 'uc_product_kit', etc.).
 *   - rownum: Current row number (we are using odd and even styles).
 *   - orientation: 0 is vertical, another is horisontal.
 *   - path_module: Path to this module.
 *
 * @return string
 *   The HTML output.
 * 
 * @see uc_pic_cart_block_block()
 */
function theme_uc_pic_cart_block_item($variables) {
  //$item = NULL, $rownum = 0, $orientation = 0, $path_module = ''

  $item = $variables['item'];
  $rownum = $variables['rownum'];
  $orientation = $variables['orientation'];
  $path_module = $variables['path_module'];

  $output = '';

  if (!is_array($item) || !is_numeric($orientation) || empty($path_module)) {
    return $output;
  }
  
  // Product feature "restrict qty" is supported
  $itemdata = unserialize($item['data']);
  $restrict_feature = (isset($itemdata['restrict_qty']['qty']) && ($itemdata['restrict_qty']['qty'] == 1));

  // Remove button
  $dest_url = drupal_get_destination();
  $img_r = theme('image', array('path' => $path_module . '/img/remove_product.gif', 'title' => t('Remove'), 'alt' => 'x'));
  $removebtn = l($img_r, 'uccartpics/remove/nojs/'. $item['nid'] .'/'. $item['ciid'], array('html' => TRUE, 'attributes' => array('class' => 'use-ajax'), 'query' => $dest_url));

  if ($orientation == 0) {
    $rowclass = ($rownum % 2) ? 'odd' : 'even';
    $output .= '<tr class="'. $rowclass .'">';
    $output .= '<td rowspan=2 class="uc_pic_cart_block_td pic"><div class="uc_pic_cart_block_productimage">'. $item['img'] .'</div></td>';
    $output .= '<td class="uc_pic_cart_block_td title">'. $item['title'] .'</td>';
    $output .= '<td class="uc_pic_cart_block_td remove">'. $removebtn .'</td></tr>'."\n";

    $use_description = (variable_get('uc_pic_cart_block_show_descriptions', FALSE) && !empty($item['description']));

    if ($use_description) {
      $output .= '<tr class="'. $rowclass .'">';
      $output .= '<td colspan=2 class="uc_pic_cart_block_td desc">'. $item['description'] .'</td>';
      $output .= '</tr>';
    }

    $output .= '<tr class="'. $rowclass .'">';
    if ($use_description) {
      $output .= '<td></td>';
    }

    $output .= '<td class="uc_pic_cart_block_td qty">';
  }
  else {
    $output .= '<div class="uc_pic_cart_block_item_hor">';
    $output .= '<div class="uc_pic_cart_block_productimage">'. $item['img'] .'</div>';
  }

  $output .= theme('uc_pic_cart_block_item_qty_manage', array(
    'item' => $item,
    'restrict_feature' => $restrict_feature,
    'removebtn' => $removebtn,
    'orientation' => $orientation,
  ));

  if ($orientation == 0) {
    $output .=  '</td>';
    $output .= '<td class="uc_pic_cart_block_td sum"><span class="uc_pic_cart_block_spansum">'. $item['price'] .'</span></td>';
    $output .= '</tr>';
  }
  else {
    $output .= '<br /><span class="uc_pic_cart_block_spansum">'. $item['price'] .'</span></div>';
  }

  return $output;
}

/**
 * Theme product image if exists.
 * 
 * @param $variables
 *   An associative array containing:
 *   - image_field: array product image data
 *   - preset: image preset.
 *   - nid: node ID to create link.
 * 
 * @return string
 *   The HTML output.
 */
function theme_uc_pic_cart_block_item_image_product($variables) {
  $image = $variables['image_field'];
  $nid = $variables['nid'];
  $preset = $variables['preset'];
  
  $img = theme('image_style', array(
    'style_name' => $preset,
    'title' => $image['title'],
    'path' => $image['uri'],
    'width' => $image['width'],
    'height' => $image['height'],
    'alt' => $image['alt']
  ));
  
  if ($nid) {
    $output = l($img, 'node/'. $nid, array('html' => TRUE));
  }
  else {
    $output = $img;
  }
  
  return $output;
}

/**
 * Theme default image.
 * 
 * @param $variables
 *   An associative array containing:
 *   - path_module: Path to this module.
 *   - preset: image preset.
 *   - product: product node.
 * 
 * @return string
 *   The HTML output.
 */
function theme_uc_pic_cart_block_item_image_default($variables) {
  $path_module = $variables['path_module'];
  $product = $variables['product'];
  $preset = $variables['preset'];
  
  // theme('image_style') doesn't work with images inside modules,
  // using theme('image') instead.
  $image_dimentions = getimagesize($path_module . '/img/no_image.gif');
  $dim = array('width' => $image_dimentions[0], 'height' => $image_dimentions[1]);
  image_style_transform_dimensions($preset, $dim);
  $attributes = array('class' => 'uc_pic_cart_block_no_image');
  if (!empty($dim['width'])) {
      $attributes['width'] = $dim['width'];
  }
  if (!empty($dim['height'])) {
      $attributes['height'] = $dim['height'];
  }
  $img = theme('image', array(
    'path' => $path_module . '/img/no_image.gif',
    'alt' => t('No pic'),
    'title' => check_plain($product->title) .' ('. t('no picture available') .')',
    'attributes' => $attributes
  ));
  
  if (node_access('view', $product)) {
    $output = l($img, 'node/'. $product->nid, array('html' => TRUE));
  }
  else {
    $output = $img;
  }
  
  return $output;
}

/**
 * Theme the item qty management block.
 * 
 * @param $variables
 *   An associative array containing:
 *   - item: cart item @see theme_uc_pic_cart_block_item.
 *   - path_module: Path to this module.
 *   - restrict_feature: is product has restrict_qty feature.
 *   - orientation: 0 is vertical, another is horisontal.
 *   - removebtn: html-string for remove button.
 * 
 * @return string
 *   The HTML output.
 */
function theme_uc_pic_cart_block_item_qty_manage($variables) {
  $item = $variables['item'];
  $path_module = $variables['path_module'];
  $restrict_feature = $variables['restrict_feature'];
  $orientation = $variables['orientation'];
  $removebtn = $variables['removebtn'];
  
  $output = '';
  
  // Product feature "restrict qty" is supported, so we can add "inc." and "dec." buttons
  // only if this feature is not used for this product.
  if ($restrict_feature) {
    $output = '<span class="uc_pic_cart_block_spanqty">'. $item['qty'] .'</span>';
    if ($orientation != 0) {
      $output .= $removebtn;
    }
  }
  else {
    $dest_url = drupal_get_destination();
    // Decrement button
    $img_d = theme('image', array('path' => $path_module . '/img/dec_product.gif', 'title' => t('Decrease'), 'alt' => '-'));
    $decbtn = l($img_d, 'uccartpics/update/nojs/dec/'. $item['nid'] .'/'. $item['ciid'], array('html' => TRUE, 'attributes' => array('class' => 'use-ajax'), 'query' => $dest_url));
    // Increment button
    $img_i = theme('image', array('path' => $path_module . '/img/inc_product.gif', 'title' => t('Increase'), 'alt' => '+'));
    $incbtn = l($img_i, 'uccartpics/update/nojs/inc/'. $item['nid'] .'/'. $item['ciid'], array('html' => TRUE, 'attributes' => array('class' => 'use-ajax'), 'query' => $dest_url));
    
    if (variable_get('uc_pic_cart_block_increase_first', FALSE)) {
      $output = $incbtn .'<span class="uc_pic_cart_block_spanqty">'. $item['qty'] .'</span>'. $decbtn;
    }
    else {
      $output = $decbtn .'<span class="uc_pic_cart_block_spanqty">'. $item['qty'] .'</span>'. $incbtn;
    }
  }
  
  return $output;
}

/**
 * Theme the block summary.
 *
 * @param $variables
 *   An associative array containing:
 *   - count: How many items are in cart.
 *   - total: How much money we want to spend :)
 * 
 * @return string
 *   The HTML output.
 */
function theme_uc_pic_cart_block_summary($variables) {
  //$count = 0, $total = 0

  $count = $variables['count'];
  $total = $variables['total'];

  if (($count == 0) || ($total == 0)) {
    return '';
  }

  $item_text = format_plural($count, '<span class="num-items">@count</span> Item', '<span class="num-items">@count</span> Items');
  $output = '<table class="uc_pic_cart_block_summary"><tbody><tr>'
  .'<td class="uc_pic_cart_block_summary_tditems">'. $item_text .'</td>'
  .'<td class="uc_pic_cart_block_summary_tdtotal">'. uc_currency_format($total) .'</td></tr>'."\n";

  $attr = array('attributes' => array('rel' => 'nofollow'));
  $output .= '<tr class="uc_pic_cart_block_summary_links"><td class="uc_pic_cart_block_summary_tdview">';
  $output .= l(t('View cart'), 'cart', $attr) .'</td><td class="uc_pic_cart_block_summary_tdcheckout">';
  if (variable_get('uc_checkout_enabled', TRUE)) {
    $output .= l(t('Checkout'), 'cart/checkout', $attr) .'</td></tr>';
  }
  else {
    $output .= '</td></tr>';
  }

  $output .= "</tbody></table>\n";

  return $output;
}

/**
 * Theme the empty cart content.
 * 
 * @param $variables
 *   An associative array containing:
 *   - path_module: Path to this module.
 * 
 * @return string
 *   The HTML output.
 */
function theme_uc_pic_cart_block_content_empty($variables) {
  $output = '<p>'. t('The cart is empty') .'</p>';
  return $output;
}
